FDRPG_DIR = ../freedroid-src
SRC_DIR := $(abspath $(FDRPG_DIR))

SUBDIRS = font floor_tiles obstacles

all:
	@if test " $(FDRPG_DIR)" = " "; then \
	  echo "Run with 'make FDRPG_DIR=<fdrpg_top_srcdir>'"; \
	  exit 1; \
	fi; \
	if ! test -d $(SRC_DIR)/data; then \
	  echo "$(SRC_DIR)/data not found."; \
	  echo "Run with 'make FDRPG_DIR=<fdrpg_top_srcdir>'"; \
	  exit 1; \
	fi; \
	rm -rf ./build; \
	mkdir ./build; \
	for subdir in $(SUBDIRS); do \
	  make -C $$subdir FDRPG_DIR=$(SRC_DIR); \
	done

install:
	@for subdir in $(SUBDIRS); do \
	  make -C $$subdir FDRPG_DIR=$(SRC_DIR) install; \
	done

clean:
	rm -rf ./build
